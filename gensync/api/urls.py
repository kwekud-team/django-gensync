from django.urls import path, include

from rest_framework import routers
from gensync.api.views import SyncStationViewSet, SyncLogViewSet, SyncObjectViewSet


router = routers.DefaultRouter()
router.register('syncstation', SyncStationViewSet)
router.register('synclog', SyncLogViewSet)
router.register('syncobject', SyncObjectViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
