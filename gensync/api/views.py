from rest_framework import viewsets
from drfbulk.viewset import DrfBulkViewSet

from gensync.models import SyncStation, SyncLog, SyncObject
from gensync.api.serializers import SyncStationSerializer, SyncLogSerializer, SyncObjectSerializer


class SyncStationViewSet(viewsets.ModelViewSet, DrfBulkViewSet):
    queryset = SyncStation.objects.all()
    serializer_class = SyncStationSerializer


class SyncLogViewSet(viewsets.ModelViewSet, DrfBulkViewSet):
    queryset = SyncLog.objects.all()
    serializer_class = SyncLogSerializer

    def get_router_for_drf(self):
        from gensync.api.urls import router
        return router


class SyncObjectViewSet(viewsets.ModelViewSet, DrfBulkViewSet):
    queryset = SyncObject.objects.all()
    serializer_class = SyncObjectSerializer
