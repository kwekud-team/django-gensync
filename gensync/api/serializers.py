from rest_framework import serializers

from gensync.models import SyncStation, SyncLog, SyncObject
from gensync.attrs import SyncStationAttr, SyncLogAttr, SyncObjectAttr


class SyncStationSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = SyncStation
        fields = SyncStationAttr.serializer_fields


class SyncLogSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = SyncLog
        fields = SyncLogAttr.serializer_fields


class SyncObjectSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = SyncObject
        fields = SyncObjectAttr.serializer_fields
