import time
import hashlib
import requests
from dataclasses import dataclass
from django.utils import timezone

from kommons.utils.generic import jsonify
from drfbulk.constants import DrfBulkK
from gensync.models import SyncStation, SyncLog, SyncObject, SyncAction
from gensync.constants import GenSyncK
from kommons.utils.data_types import Queue


@dataclass
class SyncItem:
    action: str
    local_content_str: str
    local_content_guid: str
    remote_content_str: str
    remote_content_guid: str
    extra: dict
    activity_date: str = ''

    def get_activity_date(self) -> str:
        return self.activity_date or str(timezone.now())

    def get_remote_content_guid(self) -> str:
        return self.remote_content_guid or GenSyncK.BLANK_GUID.value


class SyncActionUtils:

    @staticmethod
    def save_error(sync_action, enum_message, start_time, update_sync_content=True):
        sync_action_qs = SyncAction.objects.filter(pk=sync_action.pk)
        sync_object_qs = SyncObject.objects.filter(pk=sync_action.sync_object.pk)

        now = timezone.now()
        end_time = time.time()
        sync_action_qs.update(status_code=enum_message.code_only(),
                              status_message=enum_message.value,
                              time_taken=end_time - start_time,
                              date_completed=now,
                              date_cancelled=now)

        if update_sync_content:
            error_code = str(enum_message.code_only())
            error_prefix = GenSyncK.ERROR_GUID.value

            sync_object_qs.update(
                local_content_guid=error_prefix + error_code.zfill(12),
            )


class SyncUtils:

    def __init__(self):
        self.queue = Queue()

    @staticmethod
    def get_sync_station(remote_station_guid):
        qs = SyncStation.objects.filter(remote_station_guid=remote_station_guid)
        return qs[0] if qs.exists() else None

    def get_sync_log(self, sync_station):
        unique_key = self._generate_synclog_key()
        return SyncLog.objects.get_or_create(sync_station=sync_station, unique_key=unique_key,
                                             direction=GenSyncK.DIRECTION_FROM.value)[0]

    def commit_sync_objects(self, remote_station_guid):
        sync_station = self.get_sync_station(remote_station_guid)
        if sync_station:
            sync_log = self.get_sync_log(sync_station)
            self._save_sync_objects(sync_log)

    def _generate_synclog_key(self):
        # This is required to make sure sync logs are not duplicated for similar objects
        guid_xs = ','.join([x.local_content_guid for x in self.queue.queue_list])
        return hashlib.md5(guid_xs.encode('utf-8')).hexdigest()

    def _save_sync_objects(self, sync_log):
        for x in self.queue.queue_list:
            SyncObject.objects.get_or_create(sync_log=sync_log,
                                             local_content_str=x.local_content_str,
                                             local_content_guid=x.local_content_guid,
                                             remote_content_str=x.remote_content_str,
                                             remote_content_guid=x.get_remote_content_guid(),
                                             action=x.action,
                                             defaults={
                                                 'activity_date': x.get_activity_date(),
                                                 'extra': x.extra
                                             })


class SyncRemoteUtils:

    def __init__(self, sync_log):
        self.sync_log = sync_log
        self.sync_objects = sync_log.syncobject_set.filter(date_synced__isnull=True).exclude(
            local_content_guid=GenSyncK.BLANK_GUID.value)

    def sync_remote_objects(self):
        start_time = time.time()

        prepared_data = self._prepare_sync_data()
        response = self._push_to_remote(prepared_data)

        end_time = time.time()

        self._after_remote_sync(response, end_time - start_time)

        return response

    def _prepare_sync_data(self):
        children = []
        # If obj has remote_guid, it means it was synced from a remote location. We have to update the remote record
        # instead of creating a new one
        for x in self.sync_objects:
            if x.remote_guid:
                unique = {'guid': str(x.remote_guid)}
            else:
                unique = {'remote_guid': str(x.guid)}

            children.append({
                DrfBulkK.FIELDS_UNIQUE.value: unique,
                DrfBulkK.FIELDS_OPTIONAL.value: {
                    'remote_guid': str(x.guid),
                    'local_content_str': x.remote_content_str,
                    'remote_content_str': x.local_content_str,
                    'remote_content_guid': str(x.local_content_guid),
                    'action': x.action,
                    'local_content_guid': str(x.remote_content_guid),
                    'activity_date': str(timezone.now()),
                    'extra': x.extra
                }
            })

        if self.sync_log.remote_guid:
            unique = {'guid': str(self.sync_log.remote_guid)}
        else:
            unique = {'remote_guid': str(self.sync_log.guid)}

        data = {
            DrfBulkK.FIELDS_UNIQUE.value: unique,
            DrfBulkK.FIELDS_OPTIONAL.value: {
                'remote_guid': str(self.sync_log.guid),
                'sync_station__guid': str(self.sync_log.sync_station.remote_station_guid),
                'unique_key': self.sync_log.unique_key,
                'direction': GenSyncK.DIRECTION_TO.value,
            },
            DrfBulkK.FIELDS_CHILDREN.value: {
                DrfBulkK.FIELDS_PARENT_FIELD_NAME.value: 'sync_log',
                DrfBulkK.FIELDS_APP_MODEL.value: 'gensync.syncobject',
                DrfBulkK.FIELDS_OBJECTS.value: children,
            }
        }

        return data

    def _push_to_remote(self, data):
        url = '%s%s' % (self.sync_log.sync_station.remote_site.domain, GenSyncK.API_SINGLE_CREATE_SYNC.value)
        req = requests.post(url, json=data)
        # req = requests.post(url, data=json.dumps([data]), headers={"Content-Type": "application/json"})

        return {
            'status_code': req.status_code,
            'content': req.content,
            'serialized_obj': jsonify(req.content)
        }

    @staticmethod
    def quick_process(resp_data):
        status_code = resp_data.get('status_code', None)
        return {
            'obj': resp_data.get('serialized_obj', {}),
            'is_valid': str(status_code).startswith('2'),
            'status_code': status_code
        }

    def _after_remote_sync(self, response, time_taken):

        processed_x = self.quick_process(response)
        if processed_x['is_valid']:
            for y in processed_x['obj'].get(DrfBulkK.FIELDS_CHILDREN.value, []):
                processed_y = self.quick_process(y)
                if processed_y['is_valid']:
                    qs = SyncObject.objects.filter(guid=processed_y['obj'].get('remote_guid'))
                    qs.update(date_synced=timezone.now())

        not_synced_qs = self.sync_log.syncobject_set.filter(date_synced__isnull=True)
        if not_synced_qs.exists():
            date_completed = None
        else:
            date_completed = timezone.now()

        SyncLog.objects.filter(pk=self.sync_log.pk).update(date_completed=date_completed, time_taken=time_taken)



