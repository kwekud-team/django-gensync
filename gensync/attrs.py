
class AbstractAttr:
    serializer_fields = ['url', 'id', 'pk', 'guid', 'date_created', 'date_modified', 'is_active']


class SyncStationAttr:
    admin_list_display = ('get_name', 'guid', 'local_site', 'remote_site', 'remote_api_key', 'remote_station_guid',)
    admin_list_filter = ('is_active',)
    serializer_fields = AbstractAttr.serializer_fields + ['remote_api_key', 'remote_station_guid']


class SyncLogAttr:
    admin_list_display = ('sync_station', 'activity_date', 'unique_key', 'direction', 'time_taken', 'date_completed')
    admin_list_filter = ('sync_station', 'direction', 'date_completed',)
    admin_search_fields = ('unique_key', 'guid',)
    admin_fieldsets = [
        ('Basic', {'fields': ('sync_station', 'activity_date',)}),
        ('Extra', {'fields': ['time_taken', 'direction']}),
    ]
    serializer_fields = AbstractAttr.serializer_fields + ['sync_station', 'unique_key', 'remote_guid', 'activity_date',
                                                          'time_taken', 'direction']


class SyncObjectAttr:
    admin_list_display = ('sync_log', 'activity_date', 'action', 'local_content_str', 'local_content_guid',
                          'remote_content_str', 'remote_content_guid', 'date_synced', 'guid', 'remote_guid')
    admin_list_filter = ('action', 'local_content_str', 'remote_content_str', 'date_synced')
    admin_search_fields = ('guid', 'local_content_guid', 'remote_content_guid',)
    serializer_fields = AbstractAttr.serializer_fields + ['sync_log', 'activity_date', 'action', 'remote_guid', 'extra',
                                                          'local_content_str', 'local_content_guid',
                                                          'remote_content_str', 'remote_content_guid', ]


class SyncActionAttr:
    admin_list_display = ['sync_object', 'attempt_count', 'time_taken', 'date_completed', 'date_validated',
                          'date_cancelled', 'status_code', 'status_message']
    admin_list_filter = ['date_completed', 'date_validated', 'date_cancelled', 'status_code']
    admin_search_fields = ('guid', 'sync_object__local_content_guid', 'sync_object__remote_content_guid',)


class SyncJobAttr:
    admin_list_display = ['content_str', 'job_type', 'sync_station', 'kwargs']
    admin_list_filter = ['is_active']


class SyncJobActivityAttr:
    admin_list_display = ['sync_job', 'date_started', 'date_completed', 'response']
    admin_list_filter = ['date_started', 'date_completed']
