from django.core.management.base import BaseCommand

from gensync.jobs import BaseJobProcessor, JobRemoteProcessor
from gensync.constants import GenSyncK


class Command(BaseCommand):
    help = 'Process all pending jobs'

    def handle(self, *args, **options):
        message = BaseJobProcessor.bulk_execute(GenSyncK.JOB_TYPE_REMOTE_SYNC.value, JobRemoteProcessor)
        self.stdout.write(self.style.SUCCESS(message))
