from django.core.management.base import BaseCommand

from gensync.jobs import BaseJobProcessor, JobLocalProcessor
from gensync.constants import GenSyncK


class Command(BaseCommand):
    help = 'Process all pending picture jobs'

    def handle(self, *args, **options):
        message = BaseJobProcessor.bulk_execute(GenSyncK.JOB_TYPE_LOCAL_ACTIONS.value, JobLocalProcessor)
        self.stdout.write(self.style.SUCCESS(message))
