import django.dispatch


sig_gensync_execute_local_actions = django.dispatch.Signal(providing_args=["sync_action", "kwargs"])
sig_gensync_execute_remote_sync = django.dispatch.Signal(providing_args=["sync_log", "kwargs"])
