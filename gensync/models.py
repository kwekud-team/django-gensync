import uuid
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils import timezone

from gensync.constants import GenSyncK


class AbstractModel(models.Model):
    guid = models.UUIDField(null=True, blank=True, unique=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    objects: models.Manager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        return super(AbstractModel, self).save(*args, **kwargs)


class SyncStation(AbstractModel):
    local_site = models.ForeignKey('sites.Site', on_delete=models.CASCADE, related_name='syncstation_localsite')
    remote_site = models.ForeignKey('sites.Site', on_delete=models.CASCADE, related_name='syncstation_remotesite')
    remote_api_key = models.CharField(max_length=100, null=True, blank=True)
    remote_station_guid = models.UUIDField(null=True, blank=True)

    def __str__(self):
        return self.get_name()

    def get_name(self):
        return self.remote_site.name
    get_name.short_description = 'Site name'


class SyncLog(AbstractModel):
    sync_station = models.ForeignKey(SyncStation, on_delete=models.CASCADE)
    activity_date = models.DateTimeField(default=timezone.now)
    unique_key = models.CharField(max_length=250, null=True, blank=True)
    time_taken = models.PositiveIntegerField(default=0)
    direction = models.CharField(max_length=20)
    date_completed = models.DateTimeField(null=True, blank=True)
    remote_guid = models.UUIDField(null=True, blank=True)

    def __str__(self):
        return '%s: %s' % (self.activity_date, self.unique_key)


class SyncObject(AbstractModel):
    sync_log = models.ForeignKey(SyncLog, on_delete=models.CASCADE)
    activity_date = models.DateTimeField(default=timezone.now)
    action = models.CharField(max_length=100, null=True, blank=True)
    remote_guid = models.UUIDField(null=True, blank=True)
    local_content_str = models.CharField(max_length=50)
    local_content_guid = models.UUIDField()
    remote_content_str = models.CharField(max_length=50)
    remote_content_guid = models.UUIDField(null=True, blank=True)
    extra = JSONField(default=dict, blank=True)
    date_synced = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s -> %s' % (self.local_content_str, self.remote_content_str)

    def save(self, *args, **kwargs):
        super(SyncObject, self).save(*args, **kwargs)

        if self.action:
            SyncAction.objects.get_or_create(sync_object=self)


class SyncAction(AbstractModel):
    sync_object = models.OneToOneField(SyncObject, on_delete=models.CASCADE)
    attempt_count = models.PositiveIntegerField(default=0)
    time_taken = models.FloatField(default=0)
    date_completed = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    date_cancelled = models.DateTimeField(null=True, blank=True)
    status_code = models.PositiveIntegerField(default=0)
    status_message = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return '%s (%s)' % (self.sync_object.action, self.date_created)

    class Meta:
        ordering = ('-date_created',)


JOB_TYPES = (
    (GenSyncK.JOB_TYPE_LOCAL_ACTIONS.value, 'Process Local Actions'),
    (GenSyncK.JOB_TYPE_REMOTE_SYNC.value, 'Process Remote Sync'),
)


class SyncJob(AbstractModel):
    sync_station = models.ForeignKey(SyncStation, on_delete=models.CASCADE)
    content_str = models.CharField(max_length=100)
    job_type = models.CharField(max_length=100, choices=JOB_TYPES)
    kwargs = JSONField(default=dict, blank=True)

    def __str__(self):
        return '%s (%s)' % (self.content_str, self.date_created)

    class Meta:
        ordering = ('-date_created',)


class SyncJobActivity(models.Model):
    sync_job = models.ForeignKey(SyncJob, on_delete=models.CASCADE)
    date_started = models.DateTimeField()
    date_completed = models.DateTimeField()
    response = JSONField(default=dict, blank=True)

    def __str__(self):
        return '%s (%s)' % (self.date_started, self.date_completed)

    class Meta:
        ordering = ('-date_started',)
