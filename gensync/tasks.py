from webapp.codes.conf.celery_config import app

from gensync.constants import GenSyncK
from gensync.jobs import BaseJobProcessor, JobLocalProcessor, JobRemoteProcessor


@app.task(bind=True, name='gensync.tasks.bgr_process_local_actions')
def bgr_process_local_actions(self):
    resp = BaseJobProcessor.bulk_execute(GenSyncK.JOB_TYPE_LOCAL_ACTIONS.value, JobLocalProcessor)
    return resp


@app.task(bind=True, name='gensync.tasks.bgr_process_remote_actions')
def bgr_process_remote_actions(self):
    resp = BaseJobProcessor.bulk_execute(GenSyncK.JOB_TYPE_REMOTE_SYNC.value, JobRemoteProcessor)
    return resp
