from django.utils import timezone

from gensync.models import SyncObject, SyncAction, SyncJob, SyncJobActivity
from gensync.constants import GenSyncK
from gensync.signals import sig_gensync_execute_local_actions, sig_gensync_execute_remote_sync
from gensync.utils.sync_utils import SyncRemoteUtils


class BaseJobProcessor:

    def __init__(self, sync_station, app_model, **kwargs):
        self.sync_station = sync_station
        self.app_model = app_model
        self.kwargs = kwargs

    def execute(self):
        raise NotImplemented

    @staticmethod
    def bulk_execute(job_type, job_processor_class):
        qs = SyncJob.objects.filter(job_type=job_type, is_active=True)
        if qs.exists():
            for sync_job in qs:
                date_started = timezone.now()
                job_processor = job_processor_class(sync_job.sync_station, sync_job.content_str, **sync_job.kwargs)

                results = {
                    'pending': job_processor.get_pending_queryset().count(),
                }

                job_processor.execute()

                results.update({
                    'completed': job_processor.get_completed_queryset().count(),
                    'cancelled': job_processor.get_cancelled_queryset().count(),
                    'validated': job_processor.get_validated_queryset().count()
                })

                SyncJobActivity.objects.create(
                    sync_job=sync_job,
                    date_started=date_started,
                    date_completed=timezone.now(),
                    response=results
                )

            message = "Completed process successfully"
        else:
            message = 'Please create a Sync Job record with this job_type first: %s' % GenSyncK.JOB_TYPE_LOCAL_ACTIONS.value

        return message


class JobLocalProcessor(BaseJobProcessor):
    def get_all_queryset(self):
        return SyncAction.objects.filter(sync_object__local_content_str=self.app_model)

    def get_pending_queryset(self):
        return self.get_all_queryset().filter(date_completed__isnull=True).exclude(
            sync_object__remote_content_guid=GenSyncK.BLANK_GUID.value)

    def get_completed_queryset(self):
        return self.get_all_queryset().filter(date_completed__isnull=False)

    def get_validated_queryset(self):
        return self.get_all_queryset().filter(date_completed__isnull=False, date_validated__isnull=False)

    def get_cancelled_queryset(self):
        return self.get_all_queryset().filter(date_completed__isnull=False, date_cancelled__isnull=False)

    def execute(self):
        for sync_action in self.get_pending_queryset():
            sig_gensync_execute_local_actions.send(sender=SyncAction, sync_action=sync_action, **self.kwargs)


class JobRemoteProcessor(BaseJobProcessor):

    def get_all_queryset(self):
        return SyncObject.objects.filter(local_content_str=self.app_model)

    def get_pending_queryset(self):
        return self.get_all_queryset().filter(date_synced__isnull=True)

    def get_completed_queryset(self):
        return self.get_all_queryset().filter(date_synced__isnull=False)

    def get_validated_queryset(self):
        return self.get_all_queryset().none()

    def get_cancelled_queryset(self):
        return self.get_all_queryset().none()

    def execute(self):
        sync_log_qs = self.sync_station.synclog_set.filter(date_completed__isnull=True)
        for sync_log in sync_log_qs:
            SyncRemoteUtils(sync_log).sync_remote_objects()
            sig_gensync_execute_remote_sync.send(sender=SyncObject, sync_log=sync_log, **self.kwargs)
