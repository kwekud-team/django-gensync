# Generated by Django 2.1.8 on 2019-05-05 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gensync', '0008_syncjob_syncjobactivity'),
    ]

    operations = [
        migrations.AddField(
            model_name='syncstation',
            name='name',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
