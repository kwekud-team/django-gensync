# Generated by Django 2.1.8 on 2019-05-03 21:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gensync', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='synclog',
            name='unique_key',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
