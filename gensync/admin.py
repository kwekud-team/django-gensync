from django.contrib import admin

from gensync.models import SyncStation, SyncLog, SyncObject, SyncAction, SyncJob, SyncJobActivity
from gensync.attrs import SyncStationAttr, SyncLogAttr, SyncObjectAttr, SyncActionAttr, SyncJobAttr, SyncJobActivityAttr


@admin.register(SyncStation)
class SyncStationAdmin(admin.ModelAdmin):
    list_display = SyncStationAttr.admin_list_display
    list_filter = SyncStationAttr.admin_list_filter

    
@admin.register(SyncLog)
class SyncLogAdmin(admin.ModelAdmin):
    list_display = SyncLogAttr.admin_list_display
    list_filter = SyncLogAttr.admin_list_filter
    search_fields = SyncLogAttr.admin_search_fields


@admin.register(SyncObject)
class SyncObjectAdmin(admin.ModelAdmin):
    list_display = SyncObjectAttr.admin_list_display
    list_filter = SyncObjectAttr.admin_list_filter
    search_fields = SyncObjectAttr.admin_search_fields


@admin.register(SyncAction)
class SyncActionAdmin(admin.ModelAdmin):
    list_display = SyncActionAttr.admin_list_display
    list_filter = SyncActionAttr.admin_list_filter
    search_fields = SyncActionAttr.admin_search_fields


@admin.register(SyncJob)
class SyncJobAdmin(admin.ModelAdmin):
    list_display = SyncJobAttr.admin_list_display
    list_filter = SyncJobAttr.admin_list_filter


@admin.register(SyncJobActivity)
class SyncJobActivityAdmin(admin.ModelAdmin):
    list_display = SyncJobActivityAttr.admin_list_display
    list_filter = SyncJobActivityAttr.admin_list_filter
