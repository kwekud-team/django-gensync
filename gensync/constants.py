from enum import Enum


class GenSyncK(Enum):
    DIRECTION_TO = 'direction_to'
    DIRECTION_FROM = 'direction_from'

    # API_MULTIPLE_CREATE_SYNC = '/gensync/api/synclog/drfbulk_multiple_create/'
    API_SINGLE_CREATE_SYNC = '/gensync/api/synclog/drfbulk_single_create/'
    BLANK_GUID = '00000000-0000-0000-0000-000000000000'
    ERROR_GUID = '00000000-0000-0000-0000-'

    JOB_TYPE_LOCAL_ACTIONS = 'process_local_actions'
    JOB_TYPE_REMOTE_SYNC = 'process_remote_sync'
