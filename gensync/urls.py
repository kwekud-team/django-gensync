# -*- coding: utf-8 -*-
from django.urls import path, include
from gensync.api.urls import router


# app_name = 'gensync'

urlpatterns = [
    path('api/', include('gensync.api.urls')),
]
